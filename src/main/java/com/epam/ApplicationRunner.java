package com.epam;

import com.epam.view.MainView;

public class ApplicationRunner {
    public static void main(String[] args) {
        new MainView().run();
    }
}
