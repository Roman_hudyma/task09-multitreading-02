package com.epam.model;


import com.epam.comtroller.Command;

public class MenuHolder {

    private String menuText;
    private Command command;

    public MenuHolder(String menuText, Command command) {
        this.menuText = menuText;
        this.command = command;
    }

    public String getMenuText() {
        return menuText;
    }

    public void setMenuText(String menuText) {
        this.menuText = menuText;
    }

    public Command getCommand() {
        return command;
    }

    public void setCommand(Command command) {
        this.command = command;
    }
}

