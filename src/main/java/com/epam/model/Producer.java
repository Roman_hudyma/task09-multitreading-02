package com.epam.model;

import java.util.concurrent.LinkedBlockingQueue;

public class Producer implements Runnable {

  private LinkedBlockingQueue<Character> characterQueue;

  public Producer() {
    characterQueue = new LinkedBlockingQueue<>();
  }

  @Override
  public void run() {
    try {
      String string = "Hello from Producer";
      char[] characters = string.toCharArray();
      for (int i = 0; i < characters.length; i++) {
        characterQueue.put(characters[i]);
      }
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
  }

  public LinkedBlockingQueue<Character> getBlockingQueue() {
    return characterQueue;
  }
}
