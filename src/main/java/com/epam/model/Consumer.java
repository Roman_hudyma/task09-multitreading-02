package com.epam.model;

import java.util.concurrent.LinkedBlockingQueue;

public class Consumer implements Runnable {

  private LinkedBlockingQueue<Character> characterQueue;

  public Consumer(LinkedBlockingQueue linkedBlockingQueue) {
    this.characterQueue = linkedBlockingQueue;
  }

  @Override
  public void run() {
    while (!characterQueue.isEmpty()) {
      Character character = characterQueue.poll();
      if (character != null) {
        System.out.print(character);
      }
    }
    System.out.println();
  }
}
