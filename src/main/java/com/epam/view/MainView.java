package com.epam.view;

import com.epam.comtroller.BlockingQueueController;
import com.epam.comtroller.MultiSync;
import com.epam.comtroller.MyReadWriteLock;
import com.epam.model.MenuHolder;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;

public class MainView {
    private static final Logger logger = LogManager.getLogger();
    private static List<MenuHolder> menu = new ArrayList<>();
    private static ResourceBundle messages = ResourceBundle.getBundle("command");

    public void run() {
        createMenu();
        while (true) {
            choice();
        }
    }

    private static void choice() {
        System.out.println();
        for (int i = 0; i < menu.size(); i++) {
            System.out.println(i + 1 + " " + menu.get(i).getMenuText());
        }
        int index = -1;
        try {
            Scanner scanner = new Scanner(System.in);
            index = scanner.nextInt() - 1;
        } catch (InputMismatchException e) {
            logger.error(e);
            choice();
        }
        if (index < 0 || index > menu.size()) {
            choice();
        }
        menu.get(index).getCommand().run();
    }

    private static void createMenu() {
       menu.add(new MenuHolder(messages.getString("command.1"), new MultiSync()));
        menu.add(new MenuHolder(messages.getString("command.2"), new BlockingQueueController()));
        menu.add(new MenuHolder(messages.getString("command.3"), new MyReadWriteLock<>()));

    }

}

