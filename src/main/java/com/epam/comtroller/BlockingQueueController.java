package com.epam.comtroller;

import com.epam.model.Consumer;
import com.epam.model.Producer;

import java.util.concurrent.LinkedBlockingQueue;

public class BlockingQueueController implements Command{
    public void run() {
        Producer producer = new Producer();
        LinkedBlockingQueue<Character> characterLinkedBlockingQueue = producer.getBlockingQueue();

        Consumer consumer = new Consumer(characterLinkedBlockingQueue);

        Thread thread1 = new Thread(producer);
        Thread thread2 = new Thread(consumer);

        thread1.start();
        thread2.start();

        try {
            thread1.join();
            thread2.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
