package com.epam.comtroller;

public interface Command {

    void run();

}
