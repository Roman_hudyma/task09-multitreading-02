package com.epam.comtroller;

import java.util.concurrent.locks.ReentrantLock;

public class MultiSync implements Command {
    private final ReentrantLock locker = new ReentrantLock();
    private final Object syncA = new Object();
    private final Object syncB = new Object();
    private final Object syncC = new Object();

    public void run() {
        new Thread(this::printA).start();
        new Thread(this::printB).start();
        new Thread(this::printC).start();
    }

    public void printA(){
        locker.lock();
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("A");
        locker.unlock();
    }

    public void printB(){
        locker.lock();
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("B");
        locker.unlock();
    }

    public void printC(){
        locker.lock();
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("C");
        locker.unlock();
    }

}
